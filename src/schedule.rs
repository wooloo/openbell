use crate::pattern;

use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Schedule {
	pub name: String,        // Name of the schedule type
	pub start: Option<Time>, // Seconds since the start of the day
	pub end: Option<Time>,   // Seconds since the start of the day
	pub periods: Vec<Period>,
}
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Period {
	pub name: String,
	pub index: i8,
	pub start: Time,
	pub end: Time,
}
pub type Time = [u8; 2];

impl Schedule {
	pub fn get<'a>(p: &'a pattern::Pattern) -> usize {
		p.now()
	}
	pub fn get_by_date<'a>(p: &'a pattern::Pattern, month: u8, day: u8, weekday: usize) -> usize {
		p.date(month, day, weekday)
	}
}
