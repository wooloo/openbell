// This file abstracts the pattern file

use crate::schedule;
use chrono::{Datelike, Local};
use schedule::Schedule;
use serde::{Deserialize, Serialize};
use serde_json::from_str;

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Pattern {
	pub weekdays: Vec<usize>,
	pub special: Vec<Special>,
	pub schedules: Vec<Schedule>,
}
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Special {
	pub name: String,
	pub month: Option<u8>,
	pub day: Option<u8>,
	pub weekday: Option<u8>,
	pub schedule: u8,
}

const DEFAULT_PATTERN: &str = include_str!("../pattern.json");

impl Pattern {
	pub fn get(path: Option<String>) -> Pattern {
		let path = path.unwrap_or("./pattern.json".to_string());
		let file = std::fs::read_to_string(path).unwrap_or(DEFAULT_PATTERN.to_string());
		// println!("{}", file);
		let pattern: Pattern = match from_str(&file) {
			Ok(x) => x,
			Err(e) => {
				panic!("Pattern load failed with error {}", e);
			}
		};
		pattern
	}
	pub fn now(&self) -> usize {
		let now = Local::now();
		let month = now.month() as u8;
		let day = now.day() as u8;
		let weekday: usize = (now.weekday().number_from_monday()) as usize - 1;
		self.date(month, day, weekday)
	}
	pub fn date(&self, month: u8, day: u8, weekday: usize) -> usize {
		for i in &self.special {
			let month_match = match i.month {
				Some(m) => m == month,
				None => true,
			};
			let day_match = match i.day {
				Some(d) => d == day,
				None => true,
			};
			if day_match && month_match {
				return i.schedule as usize;
			}
		}
		self.weekdays[weekday]
	}
}

#[cfg(test)]
mod test {
	use crate::*;
	#[test]
	fn test_loads_pattern() {
		let _ = Pattern::get(Some("./pattern.json".to_string()));
	}
}
