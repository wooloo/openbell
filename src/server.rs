#![feature(proc_macro_hygiene, decl_macro)]

use rocket_contrib::json::Json;

#[macro_use]
extern crate rocket;

pub mod pattern;
pub mod schedule;

pub use pattern::{Pattern, Special};
pub use schedule::{Period, Schedule, Time};

fn main() {
	rocket::ignite()
		.mount(
			"/",
			routes![
				get_schedule_by_date,
				get_schedule_list,
				get_schedule_by_index,
				get_schedule_by_time,
				get_pattern,
				schedule_docs,
				get_license
			],
		)
		.launch();
}

#[get("/")]
fn schedule_docs() -> &'static str {
	"
	/
	/list
	/<month>/<day>/<weekday>
	/<index>
	/today
	/pattern
	/license
	"
}

#[get("/list")]
fn get_schedule_list() -> Json<Vec<Schedule>> {
	let p = Pattern::get(None);
	Json(p.schedules)
}

#[get("/<month>/<day>/<weekday>")]
fn get_schedule_by_date(month: u8, day: u8, weekday: usize) -> Json<Schedule> {
	let p = Pattern::get(None);
	let s = p.date(month, day, weekday);
	Json(p.schedules[s].clone())
}

#[get("/<index>")]
fn get_schedule_by_index(index: usize) -> Json<Schedule> {
	let p = Pattern::get(None);
	Json(p.schedules[index].clone())
}

#[get("/today")]
fn get_schedule_by_time() -> Json<Schedule> {
	let p = Pattern::get(None);
	let s = p.now();
	Json(p.schedules[s].clone())
}

#[get("/pattern")]
fn get_pattern() -> Json<Pattern> {
	let p = Pattern::get(None);
	Json(p)
}

pub mod license;
#[get("/license")]
fn get_license() -> &'static str {
	license::LICENSE
}
