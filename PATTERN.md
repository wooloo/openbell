```js
{
	"weekdays": [] // For each day, starting on monday, which schedule should be used? Errors will occur if this does not have seven elements.
	"special": [ // Declare special overrides to the weekdays array, like holidays.
		{
			"name": "Holiday",
			"month": 0,
			"day": 0,
			"weekday": 0,
			"schedule": 0 // The index of the schedule that should be used.
		}
	],
	"schedules": [
		{ // This is schedule 0.
			"name": "Weekend",
			"periods": []
			 // start and end are optional
		},
		{ // This is schedule 1.
			"name": "School Day",
			"periods": [
				{
					"name": "school period",
					"start": [ 
						9, // Hours, minutes.
						0
					],
					"end": [
						4,
						0
					],
					"index": 1 // Which class the student will actually be in right now. Use negative numbers for lunch and other non-class periods.
				}
			],
			"start": [
				9,
				0
			],
			"end": [
				4,
				0
			]
		}
	]
}
```