# OpenBell

OpenBell is an API server and Rust library that serves school bell schedules in a structured and consistent way.

OpenBell uses a JSON file to store the schedule pattern. See [the example file](pattern.example.json) and [PATTERN.md](PATTERN.md) for more information. It won't compile unless it has `pattern.json` in the repository root, since it needs to pack in a default configuration.